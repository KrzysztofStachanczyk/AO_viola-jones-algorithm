import numpy as np
import random
import cv2
face_cascade = cv2.CascadeClassifier("..\\EyeRecognitionDatabase\\opencv_classifiers\\haarcascade_frontalface_default.xml")
eye_cascade = cv2.CascadeClassifier("..\\EyeRecognitionDatabase\\opencv_classifiers\\haarcascade_eye.xml")


def findAndSave(sourcePath,destPath,filename,ext):
    img = cv2.imread(sourcePath+filename+ext)
    print(sourcePath+filename+ext)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    faces = face_cascade.detectMultiScale(gray, 1.1, 3)
    for (x,y,w,h) in faces:
        cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
        roi_gray = gray[y:y+h, x:x+w]
        eyes = eye_cascade.detectMultiScale(roi_gray)

        eye_number = 0
        if len(eyes) == 2 :
            for (ex,ey,ew,eh) in eyes:

                eye_region = roi_gray[ey:ey+eh,ex:ex+ew]
                cv2.imwrite(destPath + filename + "_" +str(eye_number) + ext,eye_region)
                eye_number=eye_number+1


def generateFake(sourcePath,destPath,filename,ext,fakeForImage):
    img = cv2.imread(sourcePath+filename+ext)
    print(sourcePath+filename+ext)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    h=gray.shape[0]
    w = gray.shape[1]

    for i in range(fakeForImage):
        s=int(w/10)
        fakeX1=random.randint(1,w-s)
        fakeY1=random.randint(1,h-s)

        roi_gray = gray[fakeY1:fakeY1+s, fakeX1:fakeX1+s]
        eyes = eye_cascade.detectMultiScale(roi_gray)

        if len(eyes) != 0 :
            i=i-1
            continue

        cv2.imwrite(destPath + filename + "_" + str(i) + ext, roi_gray)

#BOID
def parseBOID():
    for i in range(0, 1520):
        fname = 'BioID_{:04d}'.format(i)
        findAndSave('..\\EyeRecognitionDatabase\\faces\\',
                    '..\\EyeRecognitionDatabase\\eyes\\BIOID\\',
                    fname,'.png')

def parseImage():
    for i in range(1, 450):
        fname = 'image_{:04d}'.format(i)
        findAndSave('..\\EyeRecognitionDatabase\\faces\\',
                    '..\\EyeRecognitionDatabase\\eyes\\Image\\',
                    fname,'.jpg')

def parseLFW():
    for i in range(1, 13233):
        fname = 'lfw{}'.format(i)
        findAndSave('..\\EyeRecognitionDatabase\\faces\\',
                    '..\\EyeRecognitionDatabase\\eyes\\lfw\\',
                    fname, '.jpg')

def parseMEDS1():
    for i in range(1, 712):
        fname = 'MEDS-I{}'.format(i)
        findAndSave('..\\EyeRecognitionDatabase\\faces\\',
                    '..\\EyeRecognitionDatabase\\eyes\\meds-I\\',
                    fname, '.jpg')

def parseMEDS2():
    for i in range(1, 1306):
        fname = 'MEDS-II{}'.format(i)
        findAndSave('..\\EyeRecognitionDatabase\\faces\\',
                    '..\\EyeRecognitionDatabase\\eyes\\meds-II\\',
                    fname, '.jpg')

def parseMuts():
    for k in ['a','b','c','d','e']:
        for i in range(1, 751):
            fname = 'muct-{}-jpg{}'.format(k,i)
            findAndSave('..\\EyeRecognitionDatabase\\faces\\',
                        '..\\EyeRecognitionDatabase\\eyes\\muct\\',
                        fname, '.jpg')

def parseOrl():
    for i in range(1, 430):
        fname = 'orl_faces{}'.format(i)
        findAndSave('..\\EyeRecognitionDatabase\\faces\\',
                    '..\\EyeRecognitionDatabase\\eyes\\orl\\',
                    fname, '.png')

def parseSD18M():
    for i in range(1, 338):
        fname = 'sd18-m{}'.format(i)
        findAndSave('..\\EyeRecognitionDatabase\\faces\\',  '..\EyeRecognitionDatabase\\eyes\\sd18-m\\',
                    fname, '.png')

def parseSD18S():
    for i in range(1, 1418):
        fname = 'sd18-s{}'.format(i)
        findAndSave('..\\EyeRecognitionDatabase\\faces\\',
                    '..\\EyeRecognitionDatabase\\eyes\\sd18-s\\',
                    fname, '.png')

def parseAligned():
    for i in range(1, 19370):
        fname = 'aligned{}'.format(i)
        findAndSave('..\\EyeRecognitionDatabase\\faces_2\\',
                    '..\\EyeRecognitionDatabase\\eyes\\aligned\\',
                    fname, '.jpg')

def parseOp():
    for i in range(1, 28204):
        fname = 'originalPics{}'.format(i)
        findAndSave('..\\EyeRecognitionDatabase\\faces_3\\',
                    '..\\EyeRecognitionDatabase\\eyes\\originalPics\\',
                    fname, '.jpg')

def parseImageFake():
    for i in range(1, 450):
        fname = 'image_{:04d}'.format(i)
        generateFake('..\\EyeRecognitionDatabase\\faces\\',
                    '..\\EyeRecognitionDatabase\\eyes\\FakeImage\\',
                    fname, '.jpg',5)

def parseBIOIDFake():
    for i in range(1, 450):
        fname = 'BioID_{:04d}'.format(i)
        generateFake('..\\EyeRecognitionDatabase\\faces\\',
                    '..\\EyeRecognitionDatabase\\eyes\\FakeBIOID\\',
                    fname, '.png',5)

def parseMuctFake():
    for k in ['a','b','c','d','e']:
        for i in range(1, 751):
            fname = 'muct-{}-jpg{}'.format(k,i)
            generateFake('..\\EyeRecognitionDatabase\\faces\\',
                        '..\\EyeRecognitionDatabase\\eyes\\FakeMuct\\',
                        fname, '.jpg',5)

#parseBOID()
#parseImage()
#parseLFW()
#parseMEDS1()
#parseMEDS2()
#parseMuts()
#parseOrl()
#parseSD18M()
#parseSD18S()
#parseAligned()
#parseOp()
#parseImageFake()
#parseBIOIDFake()
parseMuctFake()
