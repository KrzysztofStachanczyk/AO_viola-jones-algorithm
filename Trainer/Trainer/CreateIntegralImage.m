%{
    Oblicza calke na podstawie przekazanego zdjecia

    Parametry:
        - image- NxM macierz zdjecia
    Zwraca:
        - NxM macierz calki
        
%}
function [ integralImage ] = CreateIntegralImage( image )
    imageSize = size(image);
    integralImage = zeros(imageSize);

    for row = 1:imageSize(1)
        for col = 1:imageSize(2)
            integralImage(row,col) = image(row,col);
            if row > 1
                integralImage(row,col) = integralImage(row,col) + integralImage(row-1,col);
            end

            if col> 1
                integralImage(row,col) = integralImage(row,col) + integralImage(row,col-1);
            end

            if row > 1 && col >1
                integralImage(row,col) = integralImage(row,col) - integralImage(row-1,col-1);
            end
        end
    end
end
