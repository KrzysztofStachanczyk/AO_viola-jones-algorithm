%{
    Funkcja optymalizujaca klasyfikator oparty na przekazanej cesze w celu
    redukcji bledow i wyznaczenia optymalnych wartosci progu i polaryzacji

    Parametry:
        - trainingExamples - zbior uczacy zawierajacych w kolejnych
        kolumnach: wartosci badanej cechy, polaryzacje (rodzaj) 
        elementow zbiotu uczacego, weights wagi poszczegolnych elementow
        zbioru uczacego
    
    Zwraca:
        - threshold - optymalna wartosc progu
        - toggle - polaryzacja
        - error - koncowy blad rozpoznania zbioru uczacego
%}
function [ threshold,toggle,error ] = DecisionStump( trainingExamples )
    threshold=min(trainingExamples(:,1))- 1.0;
    margin = 0.0;
    error = 2.0;
    toggle = 1.0;
    
    W_p_1 = 0.0;
    W_p_m1 = 0.0;
    
    for i=1:size(trainingExamples,1)
       if trainingExamples(i,2) > 0
           W_p_1 = W_p_1 + trainingExamples(i,3);
       end
       
       if trainingExamples(i,2) < 0
           W_p_m1 = W_p_m1 + trainingExamples(i,3);
       end
    end
    
    W_m_1=0.0;
    W_m_m1=0.0;
    
    threshold_itr=threshold;
    toggle_itr = 1.0;
    margin_itr=margin;
    error_itr=error;
    j=1;
    
    while true
        
        error_p = W_m_1 + W_p_m1;
        error_m = W_p_1 + W_m_m1;
        
        
        if error_p < error_m
            error_itr=error_p;
            toggle_itr=1.0;
        else
            error_itr=error_m;
            toggle_itr=-1.0;
        end
        
        if error_itr<error || (error_itr==error && margin_itr > margin)
            error = error_itr;
            toggle = toggle_itr;
            margin= margin_itr;
            threshold=threshold_itr;
        end
        
        if j==size(trainingExamples,1)
            break;
        end
        
        j=j+1;
        
        while true
           if trainingExamples(j,2) < 0
               W_m_m1 = W_m_m1 + trainingExamples(j,3);
               W_p_m1 = W_p_m1 - trainingExamples(j,3);
           else
               W_m_1 = W_m_1 + trainingExamples(j,3);
               W_p_1 = W_p_1 - trainingExamples(j,3);
           end
           
           if j==size(trainingExamples,1)
               break;
           else
               if trainingExamples(j,1)~=trainingExamples(j+1,1)
                    break;
               end
               j=j+1;
           end
        end
        
        if j==size(trainingExamples,1)
            maximum = max(trainingExamples(:,1));
            threshold_itr = maximum + 1.0;
            margin_itr = 0;
        else
            threshold_itr = (trainingExamples(j,1)+trainingExamples(j+1,1))/2.0;
            margin_itr = trainingExamples(j+1,1) - trainingExamples(j,1);
        end
    end
end

