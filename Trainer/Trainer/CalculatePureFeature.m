%{
    Oblicza wartosc cechy (Haar-like feature) dla zadanego zdjecia

    Parametry:
        feature - wektor opisujacy ceche
        integralImage - macierz zawierajaca calke zdjecia
    Zwraca:
        score - wartosc cechy

    Zalozenia:
        Przekazana macierz musi miec rozmiar zgodny z rozmiarem dla ktorego
        generowany byl wektor cechy (w projekcie 24x24).
        
%}

function [ score ] = CalculatePureFeature( feature,integralImage )
    score = 0;

    row=feature(2);
    col=feature(3);
    w = feature(4);
    h = feature(5);

    %{
        Typ A - dwa prostokaty ustawione poziomo
    %}
    if feature(1) == 1
        s1 =  fastSumOfBox(integralImage,[row col],[row+h,col+w-1]);
        s2 =  fastSumOfBox(integralImage,[row col+w],[row+h,col+2*w]);
        score =s1-s2;
    end

    %{
       	Typ B - trzy prostokaty ustawione poziomo
    %}
    if feature(1) == 2
       s1= fastSumOfBox(integralImage,[row col],[row+h,col+w]);
       s2= fastSumOfBox(integralImage,[row col+w+1],[row+h,col+2*w]);
       s3= fastSumOfBox(integralImage,[row col+2*w+1],[row+h,col+3*w]);
       score=s1-s2 + s3;
    end

    %{
       	Typ C - dwa prostokaty ustawione pionowo
    %}
    if feature(1) == 3
        s1= fastSumOfBox(integralImage,[row col],[row+h,col+w]);
        s2= fastSumOfBox(integralImage,[row+h+1 col],[row+2*h,col+w]);
        score=s1-s2;
    end

    %{
        Typ D - trzy prostokaty ustawione pionowo
    %}
    if feature(1) == 4
        s1= fastSumOfBox(integralImage,[row col],[row+h,col+w]);
        s2= fastSumOfBox(integralImage,[row+h+1 col],[row+2*h,col+w]);
        s3= fastSumOfBox(integralImage,[row+2*h+1 col],[row+3*h,col+w]);
        score=s1-s2 + s3;
    end

    %{
        Typ E - szachownica
    %}
    if feature(1) == 5
        s1= fastSumOfBox(integralImage,[row col],[row+h,col+w]);
        s2= fastSumOfBox(integralImage,[row+h+1 col],[row+2*h,col+w]);
        s3= fastSumOfBox(integralImage,[row col+w+1],[row,col+2*w]);
        s4= fastSumOfBox(integralImage,[row+h+1 col+w+1],[row+2*h,col+2*w]);
        score=s1-s2-s3+s4;
    end
end
