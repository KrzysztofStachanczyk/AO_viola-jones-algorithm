%{
    Skrypt pomocniczy do przygotowywania zbiorow testowych
%}

path = 'C:\Users\krzys\Desktop\KRWAWICA\AO\EyeTraining_2\recognition_examples_base';
dest = 'C:\Users\krzys\Desktop\KRWAWICA\AO\EyeTraining_2\recognitions_examples_base';
Files=dir([path,'\*.(png)|(jpg)']);

for k=1:length(Files)
        fileName = Files(k).name;
        path = Files(k).folder;
        image = imread([path,'\',fileName]);

        if size(image,3)==3
            image= rgb2gray(image);
        end
        image = imresize(image,[48,48]);

         % rozszerzenie histogramu
        minimum = double(min(min(image)));
        maximum = double(max(max(image)));

        normalizedImage = 255.0*(double(image)-minimum)/(maximum-minimum);

        imwrite(uint8(normalizedImage),[dest,'\',fileName]);
end
