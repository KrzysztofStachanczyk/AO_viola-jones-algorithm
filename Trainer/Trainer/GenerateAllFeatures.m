%{
    Generuje zbior wszystkich wektorow cech "Haar-like features" 
    dla okna o podanych wymiarach (domyslnie 24x24)
    
    Parametry:
        - rowCount - wysokosc okna
        - colCount - szerokosc okna
    Zwraca:
        - features - wektory opisujace dostepne cechy w formacie:
            feature(1) - typ
            feature(2) - y - lewy gorny rog
            feature(3) - x - lewy gorny rog
            feature(4) - szerokosc prostokatow z ktorych zbudowana jest
            cecha
            feature(5) - wysokosc prostokatow z ktorych zbudowana jest
            cecha

%}
function [ features ] = GenerateAllFeatures( rowCount, colCount )
    clear d;

    i = 1;
    
    %{
        Typ A - dwa prostokaty ustawione poziomo
    %}
    for row=1:rowCount-1
        for col=1:colCount-1
            for w=1:colCount/2 - col 
                for h = 1:rowCount - row 
                    d{i,:} = [1,row,col,w,h];
                    i = i +1;
                end
            end
        end
    end
    
    %{
       	Typ B - trzy prostokaty ustawione poziomo
    %}
    for row=1:rowCount-1
        for col=1:colCount-1
            for w=1:colCount/3 - col 
                for h = 1:rowCount - row 
                    d{i,:} = [2,row,col,w,h];
                    i = i +1;
                end
            end
        end
    end
    
    %{
       	Typ C - dwa prostokaty ustawione pionowo
    %}
    for row=1:rowCount-1
        for col=1:colCount-1
            for w=1:colCount - col 
                for h = 1:rowCount/2 - row 
                    d{i,:} = [3,row,col,w,h];
                    i = i +1;
                end
            end
        end
    end
    
    %{
        Typ D - trzy prostokaty ustawione pionowo
    %}
    for row=1:rowCount-1
        for col=1:colCount-1
            for w=1:colCount - col 
                for h = 1:rowCount/3 - row 
                    d{i,:} = [4,row,col,w,h];
                    i = i +1;
                end
            end
        end
    end
    
    %{
        Typ E - szachownica
    %}
    for row=1:rowCount-1
        for col=1:colCount-1
            for w=1:colCount/2 - col 
                for h = 1:rowCount/2 - row 
                    d{i,:} = [5,row,col,w,h];
                    i = i +1;
                end
            end
        end
    end
    
    features =cell2mat(d);
end

