%{
    Wyznacza najlepszy slaby klasyfikator w biezacej iteracji ( kroku
    treningowym).

    Parametry:
        - trainingExamples - zbior wektorow opisujacych poszczegulne
            zdjecia przy pomocy cech (wiersz zawiera kolejne ich wartosci
            dla tego samego zdjecia)
        - featuresUsed - wektor w ktorym oznaczone przez 1 sa cechy juz
            wykorzystane
        - weights - wektor wag - zdjecia poprawnie klasyfikowane przez
            poprzednie slabe klasyfikatory maja mniejsza wage
        - imageTypes - wektor typow - zdjecia nalezace do pozytywnego
            zbioru uczacego (zawierajace szukany obiekt) maja wartosc 1, dla
            negatywnego -1
    Zwraca (elementy opisujace slaby klasyfikator
            o najlepszych osiagach w tej iteracji):
        - best_d - index najlepszej cechy nalezionej w tej iteracji
        - best_threshold - wartosc progu powiazana z cecha
        - best_toggle - polaryzacja klasyfikatora opartego na cesze
        - best_error - blad zwiazaby z klasyfikatorem
%}
function [ best_d,best_threshold,best_toggle, best_error ] = BestStump( trainingExamples,featuresUsed, weights, imageTypes )

    % alokacja buforow
    threshold_vector = zeros(size(trainingExamples,2),1);
    toggle_vector = zeros(size(trainingExamples,2),1);
    error_vector = zeros(size(trainingExamples,2),1);

    % zrownoleglona petla optymalizujaca dostepne slabe klasyfikatory
    parfor d = 1:size(trainingExamples,2)
        if featuresUsed(d)<0
            example = trainingExamples(:,d);
            example = [example imageTypes weights];
            sortedExample = sortrows(example);

            % szukanie optymalnych wartosci progu i polaryzacji dla cechy
            [ threshold_vector(d),toggle_vector(d),error_vector(d) ] = DecisionStump(sortedExample);
        end
    end

    % szukanie najlepszego slabego klasyfikatora w tej iteracji
    best_error = 10e10;
    for d = 1:size(trainingExamples,2)
       if featuresUsed(d)<0
            if best_error > error_vector(d)
                 best_d=d;
                 best_error=error_vector(d);
                 best_toggle = toggle_vector(d);
                 best_threshold=threshold_vector(d);
            end
       end
    end
end
