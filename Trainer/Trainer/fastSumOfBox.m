%{
    Oblicza sume wartosci piskeli znajdujacych sie wewnatrz prostokata 
    przy zachowaniu zlozonosci O(1) w oparciu o macierz zawierajaca 
    calke zdjecia.

    Parametry:
        - integralImage - macierz calki 
        - point1, point2 - wektory wspolrzednych przeciwleglych rogow
        prostokata
    Zwraca:
        - sum - suma wartosci pikseli wewnatrz wskazanego obszaru
    Zalozenia:
        X_2 >= X_1 , Y_2>=Y_1
%}
function [ sum ] = fastSumOfBox( integralImage,point1,point2 )
    %{
        Znajac calke zdjecia mozna obliczyc sume pikseli zawierajacych sie
        we wnetrzu protokata wykorzystujac 4 elementarne operacje:

        sum = D_IMG[Y_2,X_2] - D_IMG[Y_1,X_1] - D_IMG[Y_2,X_1] - D_IMG[Y_1,X_2]
    %}

    % crop 
    point2(1) = max(0,min(size(integralImage,1),point2(1)));
    point2(2) = max(0,min(size(integralImage,2),point2(2)));
    
    point1(1) = max(0,min(size(integralImage,1),point1(1)));
    point1(2) = max(0,min(size(integralImage,2),point1(2)));
    
    sum = integralImage(point2(1),point2(2));
    if point1(1) > 1
        sum = sum - integralImage(point1(1)-1,point2(2));
    end
    
    if point1(2) > 1
        sum = sum - integralImage(point2(1),point1(2)-1);
    end
    
    if point1(2) > 1 && point1(1) > 1
        sum = sum - integralImage(point1(1)-1,point1(2)-1);
    end
end

