%{
    Laduje zbior zdjec ze wskazanej sciezki

    Parametry:
        - images - sciezka/regex do katalogu ze zdjeciami
        - M - szerokosc docelowa
        - N - wysokosc docelowa
    Zwraca:
        - samples - wektor znormalizowanych macierzy zdjec - (zakres
            zmiennosci jasnosci to 0-1) o rozmiarach MxN
%}
function [ samples ] = LoadSampleSet( images,M,N)

    clear samples;
    Files=dir(images);
    
    for k=1:length(Files)
        fileName = Files(k).name;
        path = Files(k).folder;
        image = imread([path,'\',fileName]);
        
        % Skalowanie
        if size(image,1) ~= M && size(image,2) ~= N
           image = imresize(image,[M N]);
        end
        
        % Rozciagniecie histogramu
        minimum = double(min(min(image)));
        maximum = double(max(max(image)));
        
        normalizedImage = (double(image)-minimum)/(maximum-minimum);
       
        samples{k}=normalizedImage;
    end
end

