%{
    Oblicza wartosc cechy (Haar-like feature) dla zadanego zdjecia
    z uwzglednieniem offsetu

    Parametry:
        feature - wektor opisujacy cechy
        integralImage - macierz zawierajaca calke zdjecia
        rowOffset - offset Y
        colOffset - offset Y
        featureIndex - index cechy
    Zwraca:
        score - wartosc cechy.
%}

function [ score ] = CalculatePureFeatureWithOffset( feature,integralImage, rowOffset, colOffset,featureIndex )
    score = 0;

    row=feature(featureIndex,2)+ rowOffset;
    col=feature(featureIndex,3) + colOffset;
    w = feature(featureIndex,4);
    h = feature(featureIndex,5);

    %{
        Typ A - dwa prostokaty ustawione poziomo
    %}
    if feature(featureIndex,1) == 1
        s1 =  fastSumOfBox(integralImage,[row col],[row+h,col+w-1]);
        s2 =  fastSumOfBox(integralImage,[row col+w],[row+h,col+2*w]);
        score =s1-s2;
    end

    %{
       	Typ B - trzy prostokaty ustawione poziomo
    %}
    if feature(featureIndex,1) == 2
       s1= fastSumOfBox(integralImage,[row col],[row+h,col+w]);
       s2= fastSumOfBox(integralImage,[row col+w+1],[row+h,col+2*w]);
       s3= fastSumOfBox(integralImage,[row col+2*w+1],[row+h,col+3*w]);
       score=s1-s2 + s3;
    end

    %{
       	Typ C - dwa prostokaty ustawione pionowo
    %}
    if feature(featureIndex,1) == 3
        s1= fastSumOfBox(integralImage,[row col],[row+h,col+w]);
        s2= fastSumOfBox(integralImage,[row+h+1 col],[row+2*h,col+w]);
        score=s1-s2;
    end

    %{
        Typ D - trzy prostokaty ustawione pionowo
    %}
    if feature(featureIndex,1) == 4
        s1= fastSumOfBox(integralImage,[row col],[row+h,col+w]);
        s2= fastSumOfBox(integralImage,[row+h+1 col],[row+2*h,col+w]);
        s3= fastSumOfBox(integralImage,[row+2*h+1 col],[row+3*h,col+w]);
        score=s1-s2 + s3;
    end

    %{
        Typ E - szachownica
    %}
    if feature(featureIndex,1) == 5
        s1= fastSumOfBox(integralImage,[row col],[row+h,col+w]);
        s2= fastSumOfBox(integralImage,[row+h+1 col],[row+2*h,col+w]);
        s3= fastSumOfBox(integralImage,[row col+w+1],[row,col+2*w]);
        s4= fastSumOfBox(integralImage,[row+h+1 col+w+1],[row+2*h,col+2*w]);
        score=s1-s2-s3+s4;
    end
end
