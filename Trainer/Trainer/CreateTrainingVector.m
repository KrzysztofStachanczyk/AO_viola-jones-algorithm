%{
    Tworzy wektor treningowy na podstawie zdjecia

    Parametry:
        - features - zbior wektorow opisujacych cechy ktorych wartosci maja
            zostac wygenerowane dla zdjecia
        - imagePath - sciezka do zdjecia
        - M - szerokosc docelowa
        - N - wysokosc docelowa

    Zwracja:
        - trainingVector - wektor (poziomy) zawierajacy poszczegolne
            wartosci cech opisujacych obraz (z zachowaniem ich kolejnosci)

%}
function [ trainingVector ] = CreateTrainingVector( features,imagePath, M, N )
    image = imread(imagePath);
    if size(image,3)==3
        image = rgb2gray(image);
    end
    % skalowanie
    if size(image,1) ~= M && size(image,2) ~= N
       image = imresize(image,[M N]);
    end

    % rozszerzenie histogramu

    % minimum = double(min(min(image)));
    % maximum = double(max(max(image)));
    doubleImage = double(image);
    average = mean(mean(doubleImage));
    variance = var(var(doubleImage));
    
    normalizedImage = (doubleImage-average)/variance;
    
    % normalizedImage = (double(image)-minimum)/(maximum-minimum);
    %normalizedImage=double(image)/255.0;

    % calkowanie zdjecia
    integralOfImage = CreateIntegralImage(normalizedImage);

    % obliczanie wartosci cech
    featuresCount = size(features,1);
    trainingVector = zeros(1,featuresCount);

    for featureIndex = 1:featuresCount
        trainingVector(featureIndex) = CalculatePureFeature(features(featureIndex,:),integralOfImage);
    end
end
