%{
    Przeprowadza procesz uczenia silnego klasyfikatora.

    Parametry:
        - trainingExamples - zbior wektorow opisujacych poszczegulne
            zdjecia przy pomocy cech (wiersz zawiera kolejne ich wartosci
            dla tego samego zdjecia)
        - imageIdentifiers - wektor typow - zdjecia nalezace do pozytywnego
            zbioru uczacego (zawierajace szukany obiekt) maja wartosc 1, dla
            negatywnego -1
        - numberOfSteps - ilosc iteracji (czyli liczba slabych
            klasyfikatorow tworzacych koncowy klasyfikator)

    Zwraca (kazdy wiersz opisuje jeden slaby klasyfikator wchodzacy w sklad kaskady):
        d - indexy zastosowanych cech
        d_threshold - wartosci przypisanych cechom progow
        d_toggle - polaryzacja cech
        d_alpha - waga klasyfikatora
%}
function [ d,d_threshold, d_toggle, d_alpha ] = AdaBoostLearn( trainingExamples, imageIdentifiers, numberOfSteps)
    weights = ones(size(trainingExamples,1),1.0)/size(trainingExamples,1);

    %{
      Inicjalizacja wektorow
    %}
    d=zeros(numberOfSteps,1);
    d_threshold=zeros(numberOfSteps,1);
    d_toggle=zeros(numberOfSteps,1);
    d_alpha = zeros(numberOfSteps,1);

    %{
      Oznaczenie wszystkich haar-feature jako niewykorzystanych
    %}
    featuresUsed = ones(1,size(trainingExamples,2))*-1;

    for i=1:numberOfSteps
        disp(['[AdaBoostLearn] Begin learning iteration: ',num2str(i)]);

        % szukanie najlepszego slabego klasyfikatora w tej iteracji
        [ best_d,best_threshold,best_toggle, best_error ]= BestStump( trainingExamples,featuresUsed, weights, imageIdentifiers );
        featuresUsed(best_d)=1;

        % obliczanie bledu klasyfikacji
        errorOfFeature=0.0;
        missedCount=0;
        for t=1:size(trainingExamples,1)
            if sign(CalculateFeature(trainingExamples(t,best_d),best_threshold,best_toggle))~=sign(imageIdentifiers(t))
                errorOfFeature=errorOfFeature+weights(t);
                missedCount=missedCount+1;
            end
        end

        % dodanie klasyfikatora do kaskady
        d_threshold(i)=best_threshold;
        d(i)=best_d;
        d_toggle(i)=best_toggle;

        % Wyznaczenie wagi slabego klasyfikatora
        d_alpha(i)=0.5*log((1.0-best_error) / best_error);

        % korekta wag w celu faworyzowania zdjec ktore nie zostaly jeszcze
        % poprawnie zaklasyfikowane
        for t=1:size(trainingExamples,1)
            weights(t) = weights(t)/2.0;
            if sign(CalculateFeature(trainingExamples(t,best_d),best_threshold,best_toggle))~=sign(imageIdentifiers(t))
                weights(t)=weights(t)/errorOfFeature;
            else
                weights(t)=weights(t)/(1.0-errorOfFeature);
            end
        end
    end
end
