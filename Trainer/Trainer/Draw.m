function [I2] = Draw(I, data)
    I2 = insertShape(I, 'Rectangle', data, 'LineWidth', 1);
    imshow(I2);
end