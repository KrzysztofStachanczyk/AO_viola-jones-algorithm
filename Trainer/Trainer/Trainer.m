%{
    Skrypt treningowy 
%}

positiveTrainingSetDirectory = '..\..\LipsTraining_1\cascade8\positive\*.png';
negativeTrainingSetDirectory = '..\..\LipsTraining_1\cascade8\negative\*.png';

positiveTestingSetDirectory = '..\..\LipsTraining_1\benchmark\positive\*.png';
negativeTestingSetDirectory = '..\..\LipsTraining_1\benchmark\negative\*.png';
N = 24;
cascadeLen = 30;
outputFile = "..\..\LipsTraining_1\cascade8\lipsClassifier";

%{
    Inicjalizacja puli watkow do obliczen rownoleglych
%}
disp("Running thread pool");
delete(gcp('nocreate'));
parpool('local',4);

%{
    Tworzenie wektorow cech
%}
disp('Creating features');
features = GenerateAllFeatures(N,N);

%{
    Wyznaczenie wartosci cech dla elementow zbioru uczacego
%}
disp('Creating features vectors from image');
positiveSamples=CreateTrainingVectorsFromDirectory(features,positiveTrainingSetDirectory,N,N);
disp(['    Positive samples count: ',num2str(size(positiveSamples,1))]);
negativeSamples=CreateTrainingVectorsFromDirectory(features,negativeTrainingSetDirectory,N,N);
disp(['    Negative samples count: ',num2str(size(negativeSamples,1))]);

%{
    Scalanie zbiorow uczacych
%}
featureSet = [positiveSamples(:,:) ; negativeSamples(:,:)];
imageMode = [ones(size(positiveSamples,1),1); ones(size(negativeSamples,1),1)*-1];

%{
    Trening AdaBoost
%}
disp('Learning');
[ d,d_threshold, d_toggle, d_alpha ] = AdaBoostLearn(featureSet,imageMode,cascadeLen);

%}
%{
    Benchmark klasyfikatora
%}

selectedFeatures = zeros(size(d,1),5);
parfor j = 1:size(d,1)
    selectedFeatures(j,:) = features(d(j),:);
end

%{
    Wyznaczenie wartosci cech dla elementow zbioru testowego
%}
disp('Creating benchmark features vectors from image');
positiveSamples=CreateTrainingVectorsFromDirectory(selectedFeatures,positiveTestingSetDirectory,N,N);
disp(['    Positive samples count: ',num2str(size(positiveSamples,1))]);
negativeSamples=CreateTrainingVectorsFromDirectory(selectedFeatures,negativeTestingSetDirectory,N,N);
disp(['    Negative samples count: ',num2str(size(negativeSamples,1))]);


%{
    Scalanie zbiorow testowych
%}
featureSet = [positiveSamples(:,:) ; negativeSamples(:,:)];
imageMode = [ones(size(positiveSamples,1),1); ones(size(negativeSamples,1),1)*-1];


positivePasses=0;
positiveFailes=0;

negativePasses=0;
negativeFailes=0;

%{
    Benchmark
%}
for i = 1:size(featureSet,1)
    values = zeros(size(d,1));
    
    parfor j = 1:size(d,1)
        values(j)=featureSet(i,j);
    end
    
    result = Clasificate(values,d_threshold, d_toggle, d_alpha);
    
    if imageMode(i) > 0
        if result > 0
            positivePasses = positivePasses + 1;
        else
            positiveFailes=positiveFailes+1;
        end
    else
        if result < 0
            negativePasses =negativePasses + 1;
        else
            negativeFailes = negativeFailes + 1;
        end
    end

end

%{
    Statystyki
%}
disp('Verification');
disp([' Positive passed: ',num2str(positivePasses)]);
disp([' Positive failed: ',num2str(positiveFailes)]);
disp([' Negative passed: ',num2str(negativePasses)]);
disp([' Negative failed: ',num2str(negativeFailes)]);

disp('Classyficator')
disp('type   ,   x  ,   y   ,   w    ,   h  , th   , polarity ,alpha   ');


%{ 
    Zapis klasyfikatora do pliku w formie macierzy
%}
classifier = zeros(size(d,1),8);

for j = 1:size(d,1)
    disp([num2str(selectedFeatures(j,1)), '   ' , num2str(selectedFeatures(j,2)) , '   ' ,...
        num2str(selectedFeatures(j,3)), '    ', num2str(selectedFeatures(j,4)) , '   ', ...
        num2str(selectedFeatures(j,5)), '    ', num2str(d_threshold(j)) , '   ', ...
        num2str(d_toggle(j)), '    ', num2str(d_alpha(j)) ]);
    classifier(j,1) = selectedFeatures(j,1);
    classifier(j,2) = selectedFeatures(j,2);
    classifier(j,3) = selectedFeatures(j,3);
    classifier(j,4) = selectedFeatures(j,4);
    classifier(j,5) = selectedFeatures(j,5);
    classifier(j,6) = d_threshold(j);
    classifier(j,7) = d_toggle(j);
    classifier(j,8) = d_alpha(j);
end

save(outputFile,'classifier');



