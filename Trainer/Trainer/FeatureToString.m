%{
    Umozliwia wyswietlenie informacji o podanej cesze.

    Parametry:
        - feature - wektor cechy
    Zwraca:
        - string - lancuch znakowy zawierajacy tekstowa reprezentacje
            Haar-like feature'a
%}
function [ string ] = FeatureToString( feature )
    string = ['HaarLikeFeature type: ',num2str(feature(1)), ...
        ' Row: ',num2str(feature(2)),...
        ' Col: ',num2str(feature(3)),...
        ' W: ',num2str(feature(4)),...
        ' H: ',num2str(feature(5))];
end

