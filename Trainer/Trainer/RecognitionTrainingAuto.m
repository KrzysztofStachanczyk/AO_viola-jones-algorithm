clear all;

imageSrc='..\..\LipsTraining_1\recognitions_examples_base\*.png';
fromScale = 1.0;
toScale = 8.0;
scaleStep = 0.2;
collectDetections = true;
detectionsPath = '..\..\LipsTraining_1\cascade8\detections\';
collectRecognitions = false;
recognitionsPath = '..\..\LipsTraining_1\cascade8\recognitions\';
samplesPerScaleStep = 5;
N=24.0;
Files=dir(imageSrc);
samplesPerImage = 100;
frameDivFactor = 5;
        
for k=1:length(Files)
    fileName = Files(k).name;
    path = Files(k).folder;
    imagePath=[path,'\',fileName];
    
    disp(['[Recognition] Perform recognition on: ', fileName]);
    disp('   Image loading');
    
    image = imread(imagePath);
    drawImage=image;
    origImage = image;
    if size(image,3)==3
        image = rgb2gray(image);
    end
    imageSize = size(image);
    image=double(image);
    
    disp('   Calculating ROI array size');
    %calsulate ROI max size
    ROI_MAX = 0;
    for scale = fromScale:scaleStep:toScale
        roiSize = int32(N*scale);
        step = max(1,roiSize/frameDivFactor);
        for row = roiSize:step:(imageSize(1)-roiSize-5)
            for col = roiSize:step:(imageSize(2) - roiSize-5)
                ROI_MAX = ROI_MAX+1;
            end
       end  
    end
    disp(['       ROI array size',num2str(ROI_MAX)]);
    
    
    disp('   Prepare ROI areas');
    %prepare ROI areas
    ROIs = zeros(ROI_MAX,5);
    index = 1;
    for scale = fromScale:scaleStep:toScale
        roiSize = int32(N*scale);
        step = max(1,roiSize/frameDivFactor);
        for row = roiSize:step:(imageSize(1)-roiSize-5)
            for col = roiSize:step:(imageSize(2) - roiSize-5)
                ROIs(index,:)=[col,row,roiSize,roiSize,1.0];
                index = index+1;
            end
        end
    end
    
  
    
    disp('   Remove unwanted ROIs');
    % remove unwanted ROIs (with plane texture)
    for roiId=1:ROI_MAX
        if ROIs(roiId)>0
            
            windowImage = imcrop(image,ROIs(roiId,1:4));
            stdDev = var(var(windowImage));
        
            if stdDev <= 1
                ROIs(roiId,5)=-1;
            end
            
        end
    end
    
    
    disp('   Perform cascade classification');
    classifiersPower = [30;30;30;30;30;30;30;30];
    
    for classifierID = 1:size(classifiersPower,1)
            
            
            name = ['..\..\LipsTraining_1\cascade',num2str(classifierID),'\lipsClassifier'];
            clear classifier;
            x=load(name,"classifier");
            disp(['        Work on classifier: ',name]);

            %cut classifier
            classifier=x.classifier(1:classifiersPower(classifierID),:);
            
            %split collumns
            featurePatterns=classifier(:,1:5);
            threshold=classifier(:,6);
            toggle=classifier(:,7);
            alpha=classifier(:,8);
            
            
            for roiId=1:ROI_MAX
                if mod(roiId,1000)==0
                    disp(['            ROI: ',num2str(roiId),'/',num2str(ROI_MAX)]);
                end
                
                %skip rejected
                if ROIs(roiId,5)>0
                    %ROIs(roiId,1:5)
                    
                    clear windowImage;
                    windowImage = imcrop(image,ROIs(roiId,1:4));
                    size(windowImage);
                    
                    
                    average = mean(mean(windowImage));
                    stdDev = var(var(windowImage));
                    normalizedWindowImage = (windowImage-average)/stdDev;
                    imageIntegral = CreateIntegralImage(normalizedWindowImage);

                    featureValVector = zeros(classifiersPower(classifierID),1);

                    for featureIndex = 1:classifiersPower(classifierID)
                        featureValVector(featureIndex,1) = CalculatePureFeatureScaled( featurePatterns,imageIntegral,featureIndex,N );
                    end

                    result = Clasificate(featureValVector,threshold, toggle, alpha);

                    % discarde window
                    if result < 0
                       ROIs(roiId,5)=-1.0;
                    end
                    %}
                end
            end
    end
    
    
    hits=0;
    for roiId=1:ROI_MAX
        
        %skip rejected
        if ROIs(roiId,5)>0
             drawImage=insertShape(drawImage, 'Rectangle', ROIs(roiId,1:4), 'LineWidth', 1);
             hits=hits+1;
        end
        
        if mod(roiId,1000)==0
           imshow(drawImage); 
        end
    end
    
    
    if collectDetections
        disp("    Image save");
       fname = datestr(now,'mmmm-dd-yyyy-HH-MM-SS-FFF');
       imwrite(drawImage,[detectionsPath,fname,'.png']); 
    end
    
    
    if collectRecognitions
       disp("    Recognitions saving");
       step = max(hits/samplesPerImage,1);
       stepCounter=0;
       for roiId=1:ROI_MAX
         if ROIs(roiId,5)>0
            stepCounter=stepCounter+1;
            if stepCounter >= step
                stepCounter=0;
                
                fname = datestr(now,'mmmm-dd-yyyy-HH-MM-SS-FFF');
                im = imcrop(origImage,ROIs(roiId,1:4));
                im = imresize(im,[58,58]);
                imwrite(im,[recognitionsPath,fname,'.png']);
            end
         end
       end
    end
end

