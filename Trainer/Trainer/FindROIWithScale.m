%{
    Przeszukuje zdjecie w poszukiwaniu obiektow wykrywanych przez klasyfikator

    Parametry:
      image - calka zdjecia

      Opisujace klasyfikator:
        featurePatterns - wektory cech Haar-like features
        threshold - wektor progow
        toggle - wektor polaryzacji
        alpha - wektor wag slabych klasyfikatorow

      scale - wartosc skali o jaka zmieniono rozmiar klasyfikatora
      N - rozmiar klasyfikatora

    Zwraca:
      ROIs - wykryte obiekty (kolejne wiersze macierzy) w formacie [x,y,w,h]  
%}
function [ ROIs ] = FindROIWithScale( image , featurePatterns, threshold, toggle, alpha , scale ,N )

   % obliczenie rozmiaru bloku po skalowaniu
   roiSize = int32(N*scale);

   imageSize = size(image);
   featuresCount = size(featurePatterns,1);

   % skalowanie klasyfikatora -  UP scale
   scaledFeaturePatterns=int32(zeros(size(featurePatterns)));
   for index = 1:featuresCount
       scaledFeaturePatterns(index,1)=featurePatterns(index,1);

       % skalowanie cech
       scaledFeaturePatterns(index,2)=int32(scale*featurePatterns(index,2));
       scaledFeaturePatterns(index,3)=int32(scale*featurePatterns(index,3));
       scaledFeaturePatterns(index,4)=int32(scale*featurePatterns(index,4));
       scaledFeaturePatterns(index,5)=int32(scale*featurePatterns(index,5));
   end

   % prog jest zalezny od pola zatem jego wartosc dla skali S jest S^2 raza wieksza
   scaledThreshold = threshold/(scale*scale);

   q=0;

   % obliczanie wartosci cech
   featureValVector = zeros(featuresCount,1);

   % generuj potencjalne pozycje obiektow
   maxHits = (imageSize(1)-roiSize) * (imageSize(2) - roiSize);
   step = roiSize/20;

   ROItmp = zeros(maxHits,4);
   for row = roiSize:step:(imageSize(1)-roiSize-5)
       for col = roiSize:step:(imageSize(2) - roiSize-5)

            for featureIndex = 1:featuresCount
                featureValVector(featureIndex,1) = CalculatePureFeatureWithOffset(scaledFeaturePatterns,image,row,col,featureIndex);
            end

           % featureValVector
            result = Clasificate(featureValVector,scaledThreshold, toggle, alpha);

            if result > 0
               q=q+1;
               ROItmp(q,:)=[col,row,roiSize,roiSize];
            end
       end
   end

   ROIs=ROItmp(1:q,:);
end
