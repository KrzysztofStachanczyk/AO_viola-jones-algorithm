%{
    Oblicza wynik klasyfikacji z wykorzystaniem slabego klasyfikatora -
        tj. pojedynczej cechy (Haar-like feature).

    Parametry:
        - featureValue - wartosc cechy
        - threshold - prog przypisany do cechy
        - toogle - polaryzacja

    Zwraca:
        - result - wynik klasyfikacji - 1 jesli klasyfikator rozpoznal
        obiekt, -1 w przeciwnym wypadku
%}
function [ result ] = CalculateFeature( featureValue , threshold, toogle)
    if featureValue > threshold
        result=1.0;
    else
        result=-1.0;
    end

    %{
        Uwzglednienie polaryzacji - czesc klasyfikatorow sluzy do
        wykrywania czy wartosc cechy (Haar-like feature)
        charakteryzuje obiekty, ktore nie sa przynalezne do szukanej grupy.
        Dla takich klasyfikatorow polaryzacja wynosi -1
    %}
    result=result*toogle;
end
