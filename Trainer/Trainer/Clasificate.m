%{
    Oblicza wynik klasyfikacji z wykorzystaniem mocnego klasyfikatora.
    Parametry:
        - featureValues - wektor wartosci wybranych cech (Haar-Like Features)
    Wektory definiujace klasyfikator:
        - threshold - wektor progow przypisanych do poczegolnych cech
        - toggle - wektor polaryzacji poszczeg�lnych cech
        - alpha - wektor wag cech
    Zwraca:
        - result - 1 jezeli rozpoznano obiekt na podstawie
        klasyfikatora. W przeciwnym wypadku -1 .
%}
function [ result ] = Clasificate( featureValues,threshold, toggle, alpha )
    %{
        Wynik klasyfikacji silnego klasyfikatora dany jest wzorem
          sign(sum(alpha(i)*vote(i)))
    %}
    sum = 0.0;
    for i=1:size(featureValues,1)
        sum=sum+alpha(i)*CalculateFeature(featureValues(i),threshold(i),toggle(i));
    end

    result = sign(sum);
end
