%{
    Tworzy wektory treningowe w oparciu o zdjecia zawarte we wskazanym
    katalogu
    
    Parametry:
        - features - zbior wektorow opisujacych cechy ktorych wartosci maja 
            zostac wygenerowane na podstawie zdjec
        - directory - sciezka/regex do katalogu ze zdjeciami
        - M - szerokosc docelowa
        - N - wysokosc docelowa

    Zwraca: 
        - trainingSet - macierz ktorej kolejne kolumny zawieraja wartosci
            poszczegolnych cech (Haar-Like Feature's) z zachowaniem ich 
            kolejnosci (zgodnie z kolejnoscia wystepowania w zbiorze
            features), poszczegolne wiersze reprezenuja kolejne obrazy

%}
function [ trainingSet ] = CreateTrainingVectorsFromDirectory( features,directory,M,N )
    Files=dir(directory);
    
    trainingSet=zeros(length(Files),size(features,1));
    
    parfor k=1:length(Files)
        fileName = Files(k).name;
        path = Files(k).folder;
        trainingSet(k,:)=CreateTrainingVector(features,[path,'\',fileName],M,N);
    end
end

