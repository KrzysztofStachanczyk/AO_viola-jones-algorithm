clear all;
addpath(genpath('..\Trainer\'));

fileName='.\examples\26.png';
fromScale = 1.0;
toScale = 4.0;
scaleStep = 0.1;
N=24.0;
frameDivFactor = 7;

voteThreshold = 2;
marginForPostprocessing = 40;
            
disp(['[Recognition] Perform recognition on: ', fileName]);
disp('   Image loading');

image = imread(fileName);

% TO-DO resize to small res eg. 300xH with fixed aspect ratio
% then find optimal scale range and step bbox size must be in range
% 24...min(W,H)/2

drawImage=image;
origImage = image;
if size(image,3)==3
    image = rgb2gray(image);
end
imageSize = size(image);
image=double(image);

subplot(1,3,1);
imshow(drawImage); 

disp('   Calculating ROI array size');
%calsulate ROI max size
ROI_MAX = 0;
for scale = fromScale:scaleStep:toScale
    roiSize = int32(N*scale);
    step = max(1,roiSize/frameDivFactor);
    for row = roiSize:step:(imageSize(1)-roiSize-5)
        for col = roiSize:step:(imageSize(2) - roiSize-5)
            ROI_MAX = ROI_MAX+1;
        end
   end  
end
disp(['       ROI array size',num2str(ROI_MAX)]);


disp('   Prepare ROI areas');
%prepare ROI areas
ROIs = zeros(ROI_MAX,5);
index = 1;
for scale = fromScale:scaleStep:toScale
    roiSize = int32(N*scale);
    step = max(1,roiSize/frameDivFactor);
    for row = roiSize:step:(imageSize(1)-roiSize-5)
        for col = roiSize:step:(imageSize(2) - roiSize-5)
            ROIs(index,:)=[col,row,roiSize,roiSize,1.0];
            index = index+1;
        end
    end
end



disp('   Remove unwanted ROIs');
% remove unwanted ROIs (with plane texture)
for roiId=1:ROI_MAX
    if ROIs(roiId)>0

        windowImage = imcrop(image,ROIs(roiId,1:4));
        stdDev = var(var(windowImage));

        if stdDev <= 1
            ROIs(roiId,5)=-1;
        end

    end
end


disp('   Perform cascade classification');
classifiersPower = [30;30;30;30;30;30;30;30];

for classifierID = 1:size(classifiersPower,1)
        name = ['.\classifier\layer',num2str(classifierID)];
        clear classifier;
        x=load(name,"classifier");
        disp(['        Work on classifier: ',name]);

        %cut classifier
        classifier=x.classifier(1:classifiersPower(classifierID),:);

        %split collumns
        featurePatterns=classifier(:,1:5);
        threshold=classifier(:,6);
        toggle=classifier(:,7);
        alpha=classifier(:,8);


        for roiId=1:ROI_MAX
            if mod(roiId,1000)==0
                disp(['            ROI: ',num2str(roiId),'/',num2str(ROI_MAX)]);
            end

            %skip rejected
            if ROIs(roiId,5)>0
                
                clear windowImage;
                windowImage = imcrop(image,ROIs(roiId,1:4));
                size(windowImage);

                average = mean(mean(windowImage));
                stdDev = var(var(windowImage));
                normalizedWindowImage = (windowImage-average)/stdDev;
                imageIntegral = CreateIntegralImage(normalizedWindowImage);

                featureValVector = zeros(classifiersPower(classifierID),1);

                for featureIndex = 1:classifiersPower(classifierID)
                    featureValVector(featureIndex,1) = CalculatePureFeatureScaled( featurePatterns,imageIntegral,featureIndex,N );
                end

                result = Clasificate(featureValVector,threshold, toggle, alpha);

                % discard window
                if result < 0
                   ROIs(roiId,5)=-1.0;
                end
            end
        end
end

tmpImage=drawImage;
for roiId=1:ROI_MAX
    %skip rejected
    if ROIs(roiId,5)>0
         tmpImage=insertShape(tmpImage, 'Rectangle', ROIs(roiId,1:4), 'LineWidth', 1);
    end
end
subplot(1,3,2);
imshow(tmpImage); 

disp('   Postprocessing');
disp('       Remove overlapping bboxes');
% remove overlapping bboxes
votesForBBox = zeros(ROI_MAX,1);
filteredROIs = ones(ROI_MAX,1);

for parentID=1:ROI_MAX
    if ROIs(parentID,5)>0
       for childID=1:ROI_MAX
           if parentID~=childID && ROIs(childID,5)>0
               % check boundary
               if ROIs(parentID,1)<ROIs(childID,1) && ...
                    ROIs(parentID,2)<ROIs(childID,2) && ...
                    ROIs(parentID,1)+ ROIs(parentID,3)> ROIs(childID,1)+ ROIs(childID,3) && ...
                    ROIs(parentID,2)+ ROIs(parentID,4)> ROIs(childID,2)+ ROIs(childID,4)

                    filteredROIs(parentID,1)=-1;
                    votesForBBox(childID,1)=votesForBBox(childID,1)+1;
               end
           end
       end
    end
end

% save buffer
for index=1:ROI_MAX
   ROIs(index,5)=filteredROIs(index,1); 
end

% if close to each other select one (move to possition average)
% and vote for it, margin is defined as variable marginForPostprocessing
disp('       Remove bboxes in neighbourhood');
for parentID=1:ROI_MAX
    if ROIs(parentID,5)>0 && votesForBBox(parentID,1)>=voteThreshold
        avgX = ROIs(parentID,1);
        avgY = ROIs(parentID,2);
        avgA = ROIs(parentID,3);
        avgB = ROIs(parentID,4);
        i = 1;
        for childID=1:ROI_MAX
           if parentID~=childID && ROIs(childID,5)>0 && votesForBBox(childID,1)>=voteThreshold
               if abs(ROIs(parentID,1)-ROIs(childID,1)) <= marginForPostprocessing && ...
					abs(ROIs(parentID,2)-ROIs(childID,2)) <= marginForPostprocessing
                        avgX = avgX + ROIs(childID,1);
                        avgY = avgY + ROIs(childID,2);
                        avgA = avgA + ROIs(childID,3);
                        avgB = avgB + ROIs(childID,4);
                        i = i + 1;
                        votesForBBox(childID,1) = 0;
               end
           end
        end
        votesForBBox(parentID,1) = votesForBBox(parentID,1) + 1;
        ROIs(parentID,1) = avgX / i;
        ROIs(parentID,2) = avgY / i;
        ROIs(parentID,3) = avgA / i;
        ROIs(parentID,4) = avgB / i;
    end
end

hits=0;
for roiId=1:ROI_MAX
    %skip rejected
    if ROIs(roiId,5)>0 && votesForBBox(roiId,1)>=voteThreshold
         drawImage=insertShape(drawImage, 'Rectangle', ROIs(roiId,1:4), 'LineWidth', 2, 'Color', 'red');
         hits=hits+1;
    end
end
subplot(1,3,3);
imshow(drawImage); 



